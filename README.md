# Overview

Hello reviewer, as you might have noticed, this service is written in PHP with laravel. The main reason for this is because of all the three languages I can comfortably write, PHP is the closest to C#.
so I figured it will help ease our discussions during the interview.

## Getting Started.

All you need to get started is docker and git.

When setting up for the very first time, run <br>
`docker-compose up --build sms-service`

For subsequent restarts of the project, run <br>
`docker-compose up -d sms-service`

## Project structure
The service follows a DDD pattern, and a well-thought-out hexagonal architecture. Because a lot of things weren't defined, it was
very important to design something that would allow for easy swapping out of layers and parts of the implementation
without changes to the domain.

I also understand there are a lot of folders and files in this project, whereas all of them are necessary for laravel to function, they
can also shadow the important stuff. So I would like to highlight the important stuff below.

```
.
├── README.md                         # set up instructions and general info
├── docker-compose.yml                # docker containers and containers config
├── sms-service                       # minimal laravel project 
│   ├── SmsNotifier                   # This folder represents our bounded context. It contains everythng relevant to sending sms messages.
│   │   ├── Application               # Contains all our interactors like Commands, Events,Command Handlers and so on. Anything that acts on the domain.
│   │   ├── Domain                    # This represents our domain, it contains an SMS entity and a couple of domain Abstractions.
│   │   ├── UI                        # This is the Transport layer for our bounded context / domain, contains a couple of CLI jobs to consume from a queue.
└── stack                             # Contains PHP specific config files.

```

## Overview of my approach.
To put it briefly,
* We have some CLI jobs, inside the UI folder. They talk to a `queue-service-interface` to consume `send-sms-commands` from a queue,
  deserialize the payload, create an SMS entity, pass it to a `send-sms-command` and fire off the command.

* The Command's handler talks to a domain abstraction (in this case a `send-sms-service`) which "makes an HTTP call" to the external
  service and depending on the result. Fires off the appropriate events.

* All events fired off by the service above are stored (event sourced)
* The SMS failed event also triggers a different handler that re-queues the failed sms on a `failed-sms-queue`
* There is a separate job that runs on the `failed-sms-queue` to retry the failed sms messages. A message will be dropped as a bad message if it fails twice.
  This value can be configured depending on message type and business requirements.

>For the purposes of our discussion, I have created a separate branch, with concrete implementations for some stuff, so I can be able to demo the above workflow

## Running Tests

To run the testsuite, run the command below <br>
`docker-compose run sms-service composer test`
