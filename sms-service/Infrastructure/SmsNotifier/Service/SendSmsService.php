<?php declare(strict_types=1);

namespace SmsNotifier\Infrastructure\SmsNotifier\Service;

use Illuminate\Http\Client\Response as HttpResponse;
use SmsNotifier\Domain\Service\SendSmsServiceInterface;
use SmsNotifier\Domain\Entity\Sms;

class SendSmsService implements SendSmsServiceInterface
{
    public function send(Sms $sms): HttpResponse
    {
        // TODO implement send method
    }
}
