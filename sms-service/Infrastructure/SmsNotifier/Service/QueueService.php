<?php declare(strict_types=1);

namespace SmsNotifier\Infrastructure\SmsNotifier\Service;

use SmsNotifier\Domain\Entity\Sms;
use SmsNotifier\UI\CLI\QueueServiceInterface;

class QueueService implements QueueServiceInterface
{
    public function consume(string $queue): Sms
    {
        // TODO: Implement consume() method.
    }

    public function publish(string $queue, array $content, $routing = '/'): void
    {
        // TODO: Implement publish() method.
    }
}
