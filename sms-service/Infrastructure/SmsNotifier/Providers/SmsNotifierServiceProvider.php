<?php declare(strict_types=1);

namespace SmsNotifier\Infrastructure\SmsNotifier\Providers;

use Illuminate\Support\ServiceProvider;
use SmsNotifier\Application\Command\SendSmsCommand;
use SmsNotifier\Application\Handler\SendSmsHandler;
use SmsNotifier\Domain\Service\SendSmsServiceInterface;
use SmsNotifier\Infrastructure\SmsNotifier\Service\QueueService;
use SmsNotifier\Infrastructure\SmsNotifier\Service\SendSmsService;
use SmsNotifier\UI\CLI\ConsumeFailedSmsCommand;
use SmsNotifier\UI\CLI\ConsumeSendSmsCommand;
use SmsNotifier\UI\CLI\QueueServiceInterface;
use Illuminate\Bus\Dispatcher;

class SmsNotifierServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->app->extend(Dispatcher::class, static function (Dispatcher $dispatcher) {
            $dispatcher->map([
                SendSmsCommand::class => SendSmsHandler::class,
            ]);

            return $dispatcher;
        });
    }

    public function register():void
    {
        $this->app->bind(
            QueueServiceInterface::class,
            QueueService::class
        );

        $this->app->bind(
            SendSmsServiceInterface::class,
            SendSmsService::class
        );

        $this->commands([
            ConsumeSendSmsCommand::class,
            ConsumeFailedSmsCommand::class,
        ]);
    }
}
