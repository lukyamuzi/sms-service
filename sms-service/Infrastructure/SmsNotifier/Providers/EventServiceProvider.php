<?php declare(strict_types=1);

namespace SmsNotifier\Infrastructure\SmsNotifier\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use SmsNotifier\Application\Event\SmsSendingFailed;
use SmsNotifier\Application\Handler\RequeueFailedSmsHandler;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        SmsSendingFailed::class => [
            RequeueFailedSmsHandler::class,
        ],
    ];
}
