<?php declare(strict_types=1);

namespace SmsNotifier\UI\CLI;

use SmsNotifier\Domain\Entity\Sms;

interface QueueServiceInterface
{
    public function consume(string $queue): Sms;

    public function publish(string $queue, array $content, $routing = '/'): void;
}
