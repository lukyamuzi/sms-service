<?php declare(strict_types=1);

namespace SmsNotifier\UI\CLI;

use Illuminate\Bus\Dispatcher;
use Illuminate\Console\Command;
use Psr\Log\LoggerInterface;
use SmsNotifier\Application\Command\SendSmsCommand;

class ConsumeFailedSmsCommand extends Command
{
    public $signature = 'consume:failed:sms:command';
    private const FAILURE_THRESHOLD = 2;

    public function __construct(
        private QueueServiceInterface $queueService,
        private Dispatcher $dispatcher,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $sms = $this->queueService->consume(env('FAILED_SMS_QUEUE'));

        if ($sms->getFailureCount() >= self::FAILURE_THRESHOLD) {
            $this->logger->critical("SMS sending failed, sms with uuid: {$sms->getUuid()} message has been dropped");
            return;
        }

        $this->dispatcher->dispatchNow(new SendSmsCommand($sms));
    }
}
