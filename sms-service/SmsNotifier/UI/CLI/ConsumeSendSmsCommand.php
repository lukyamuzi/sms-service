<?php declare(strict_types=1);

namespace SmsNotifier\UI\CLI;

use Illuminate\Bus\Dispatcher;
use Illuminate\Console\Command;
use SmsNotifier\Application\Command\SendSmsCommand;

class ConsumeSendSmsCommand extends Command
{
    public $signature = 'consume:send:sms:command';

    public function __construct(
        private QueueServiceInterface $queueService,
        private Dispatcher $dispatcher,
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $sms = $this->queueService->consume(env('SEND_SMS_QUEUE'));

        $this->dispatcher->dispatchNow(new SendSmsCommand($sms));
    }
}
