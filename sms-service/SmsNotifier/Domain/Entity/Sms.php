<?php declare(strict_types=1);

namespace SmsNotifier\Domain\Entity;

class Sms
{
    public function __construct(
        private string $uuid,
        private string $type,
        private string $phoneNumber,
        private string $messageBody,
        private int $failureCount,
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMessageBody(): string
    {
        return $this->messageBody;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getFailureCount(): int
    {
        return $this->failureCount;
    }

    public function incrementFailureCount(): void
    {
        ++ $this->failureCount;
    }

    public function toArray(): array
    {
        return [
            $this->uuid,
            $this->type,
            $this->phoneNumber,
            $this->messageBody,
            $this->failureCount,
        ];
    }
}
