<?php declare(strict_types=1);

namespace SmsNotifier\Domain\Factory;

use Illuminate\Support\Arr;
use Ramsey\Uuid\Uuid;
use SmsNotifier\Domain\Entity\Sms;

class SmsEntityFactory
{
    public static function fromArray(array $data): Sms
    {
        $uuid = Arr::get($data, 'uuid') ?? (string) Uuid::uuid4();
        $failureCount = Arr::get($data, 'failureCount') ?? 0;

        return new Sms(
            $uuid,
            Arr::get($data, 'type'),
            Arr::get($data, 'phoneNumber'),
            Arr::get($data, 'messageBody'),
            $failureCount
        );
    }
}
