<?php declare(strict_types=1);

namespace SmsNotifier\Domain\Service;

use SmsNotifier\Domain\Entity\Sms;
use Illuminate\Http\Client\Response as HttpResponse;

interface SendSmsServiceInterface
{
    public function send(Sms $sms): HttpResponse;
}
