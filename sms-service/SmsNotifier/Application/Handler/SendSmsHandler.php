<?php declare(strict_types=1);

namespace SmsNotifier\Application\Handler;

use Psr\Log\LoggerInterface;
use SmsNotifier\Application\Command\SendSmsCommand;
use SmsNotifier\Application\Event\SmsSendingFailed;
use SmsNotifier\Application\Event\SmsSendingSucceeded;
use SmsNotifier\Domain\Service\SendSmsServiceInterface;

class SendSmsHandler
{
    public function __construct(
        private SendSmsServiceInterface $sendSmsService,
        private LoggerInterface $logger
    ) {
    }

    public function handle(SendSmsCommand $command): Void
    {
        $sms = $command->getSms();
        $response = $this->sendSmsService->send($sms);

        if ($response->ok()) {
            // dispatch storable event for sms sent
            event(new SmsSendingSucceeded(
                $sms->getUuid(),
                $sms->getType(),
                $sms->getPhoneNumber(),
                $sms->getMessageBody()
            ));
        } else {
            $sms->incrementFailureCount();

            // dispatch storable event for sms failed
            event(new SmsSendingFailed(
                $sms->getUuid(),
                $sms->getType(),
                $sms->getPhoneNumber(),
                $sms->getMessageBody(),
                $sms->getFailureCount(),
                $response->status(),
                $response->body()
            ));

            $this->logger->error("SMS sending failed, sms with uuid: {$sms->getUuid()} has been re-queued");
        }
    }
}
