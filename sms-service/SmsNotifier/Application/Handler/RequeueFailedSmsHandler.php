<?php declare(strict_types=1);

namespace SmsNotifier\Application\Handler;

use SmsNotifier\Application\Event\SmsSendingFailed;
use SmsNotifier\UI\CLI\QueueServiceInterface;

class RequeueFailedSmsHandler
{
    public function __construct(private QueueServiceInterface $queueService)
    {
    }

    public function handle(SmsSendingFailed $event): void
    {
        $this->queueService->publish(env('FAILED_SMS_QUEUE'), $event->toArray());
    }
}
