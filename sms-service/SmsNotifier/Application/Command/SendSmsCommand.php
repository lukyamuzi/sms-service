<?php declare(strict_types=1);

namespace SmsNotifier\Application\Command;

use SmsNotifier\Application\Handler\SendSmsHandler;
use SmsNotifier\Domain\Entity\Sms;

/** @see SendSmsHandler */
class SendSmsCommand
{
    public function __construct(private Sms $sms)
    {
    }

    public function getSms(): Sms
    {
        return $this->sms;
    }
}
