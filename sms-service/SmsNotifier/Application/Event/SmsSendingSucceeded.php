<?php declare(strict_types=1);

namespace SmsNotifier\Application\Event;

class SmsSendingSucceeded extends StorableEvent
{
}
