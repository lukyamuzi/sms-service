<?php declare(strict_types=1);

namespace SmsNotifier\Application\Event;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

abstract class StorableEvent extends ShouldBeStored
{
    public function __construct(
        private string $uuid,
        private string $type,
        private string $phoneNumber,
        private string $messageBody
    ) {
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getMessageBody(): string
    {
        return $this->messageBody;
    }
}
