<?php declare(strict_types=1);

namespace SmsNotifier\Application\Event;

class SmsSendingFailed extends StorableEvent
{
    public function __construct(
        string $uuid,
        string $type,
        string $phoneNumber,
        string $messageBody,
        private int $failureCount,
        private int $errorCode,
        private string $errorMessage,
    ) {
        parent::__construct($uuid, $type, $phoneNumber, $messageBody);
    }

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getFailureCount(): int
    {
        return $this->failureCount;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->getUuid(),
            'type' => $this->getType(),
            'phoneNumber'=> $this->getPhoneNumber(),
            'messageBody' => $this->getmessageBody(),
            'failureCount' => $this->failureCount,
        ];
    }
}
