<?php declare(strict_types=1);

namespace Tests\Unit;

use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Testing\WithFaker;
use SmsNotifier\Application\Command\SendSmsCommand;
use SmsNotifier\Domain\Factory\SmsEntityFactory;
use SmsNotifier\UI\CLI\ConsumeSendSmsCommand;
use SmsNotifier\UI\CLI\QueueServiceInterface;
use Tests\TestCase;

class ConsumeSendSmsCommandTest extends TestCase
{
    use WithFaker;

    public function testCanDispatchSendSmsCommand(): void
    {
        $smsEntity = SmsEntityFactory::fromArray([
            'type' => 'promotional',
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
        ]);

        $this->mock(QueueServiceInterface::class)
            ->shouldReceive('consume')
            ->with(env('SEND_SMS_QUEUE'))
            ->andReturn($smsEntity);

        $this->mock(Dispatcher::class)
            ->shouldReceive('dispatchNow')
            ->with(\Mockery::type(SendSmsCommand::class))
            ->andReturnNull();

        $consumeCommand = app(ConsumeSendSmsCommand::class);
        $consumeCommand->handle();
    }
}
