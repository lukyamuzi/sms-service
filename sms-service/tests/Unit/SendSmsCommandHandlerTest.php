<?php declare(strict_types=1);

namespace Tests\Unit;

use GuzzleHttp\Psr7\Response as MockResponse;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Client\Response as HttpResponse;
use Psr\Log\LoggerInterface;
use SmsNotifier\Application\Command\SendSmsCommand;
use SmsNotifier\Application\Event\SmsSendingFailed;
use SmsNotifier\Application\Event\SmsSendingSucceeded;
use SmsNotifier\Application\Handler\SendSmsHandler;
use SmsNotifier\Domain\Factory\SmsEntityFactory;
use SmsNotifier\Domain\Service\SendSmsServiceInterface;
use Tests\TestCase;

class SendSmsCommandHandlerTest extends TestCase
{
    use WithFaker;

    public function testHandlerPublishesSmsSucceededEventOnSuccessFulHttpRequest(): void
    {
        // we expect this event to be fired
        $this->expectsEvents(SmsSendingSucceeded::class);

        // given
        $smsEntity = SmsEntityFactory::fromArray([
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
        ]);
        $command = new SendSmsCommand($smsEntity);

        $this->mock(SendSmsServiceInterface::class)
            ->shouldReceive('send')
            ->with($smsEntity)
            ->andReturn(new HttpResponse(new MockResponse(200, [], 'SMS sent successfully')));

        // when
        $handler = app(SendSmsHandler::class);
        $handler->handle($command);
    }

    public function testPublishesSmsFailedEventOnUnsuccessfulHttpRequest(): void
    {
        // we expect this event to be fired
        $this->expectsEvents(SmsSendingFailed::class);

        // given
        $smsEntity = SmsEntityFactory::fromArray([
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
        ]);
        $command = new SendSmsCommand($smsEntity);

        $this->mock(LoggerInterface::class)
            ->shouldReceive('error')
            ->with("SMS sending failed, sms with uuid: {$smsEntity->getUuid()} has been re-queued")
            ->andReturnNull();

        $this->mock(SendSmsServiceInterface::class)
            ->shouldReceive('send')
            ->with($smsEntity)
            ->andReturn(new HttpResponse(new MockResponse(503, [], 'something went wrong')));

        // when
        $handler = app(SendSmsHandler::class);
        $handler->handle($command);
    }
}
