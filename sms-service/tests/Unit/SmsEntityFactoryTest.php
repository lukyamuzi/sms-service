<?php declare(strict_types=1);

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use SmsNotifier\Domain\Factory\SmsEntityFactory;
use Tests\TestCase;

class SmsEntityFactoryTest extends TestCase
{
    use WithFaker;

    public function testCanCreateSmsValueObjectCorrectly(): void
    {
        $data = [
            'type' => $this->faker->word,
            'phoneNumber' => '0897398475',
            'messageBody' => 'we would like to confirm your payment',
        ];

        $smsEntity = SmsEntityFactory::fromArray($data);

        self::assertEquals('0897398475', $smsEntity->getPhoneNumber());
        self::assertEquals('we would like to confirm your payment', $smsEntity->getMessageBody());
        self::assertEquals(0, $smsEntity->getFailureCount());
    }

    public function testFactoryGetsTheFailureCountRight(): void
    {
        $data = [
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
            'failureCount' => 1,
        ];

        $smsEntity = SmsEntityFactory::fromArray($data);

        self::assertEquals(1, $smsEntity->getFailureCount());
    }

    public function testFactoryCanGenerateUuidWhenNothingIsPassed(): void
    {
        $data = [
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
        ];

        $smsEntity = SmsEntityFactory::fromArray($data);

        self::assertNotNull($smsEntity->getUuid());
    }

    public function testFactoryDoesntGenerateNewUuidIfOneIsPassed(): void
    {
        $data = [
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
            'uuid' => 'd28e27f3-5fad-442e-8142-5360009fd246',
        ];

        $smsEntity = SmsEntityFactory::fromArray($data);

        self::assertEquals('d28e27f3-5fad-442e-8142-5360009fd246', $smsEntity->getUuid());
    }
}
