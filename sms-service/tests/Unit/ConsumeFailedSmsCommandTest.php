<?php declare(strict_types=1);

namespace Tests\Unit;

use Illuminate\Bus\Dispatcher;
use Illuminate\Foundation\Testing\WithFaker;
use Psr\Log\LoggerInterface;
use SmsNotifier\Application\Command\SendSmsCommand;
use SmsNotifier\Domain\Factory\SmsEntityFactory;
use SmsNotifier\UI\CLI\ConsumeFailedSmsCommand;
use SmsNotifier\UI\CLI\QueueServiceInterface;
use Tests\TestCase;

class ConsumeFailedSmsCommandTest extends TestCase
{
    use WithFaker;

    public function testCanDispatchSendSmsCommandWhenRetryCountIsBelow2(): void
    {
        $smsEntity = SmsEntityFactory::fromArray([
            'uuid' => $this->faker->uuid,
            'type' => $this->faker->word,
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
            'failureCount' => 1,
        ]);

        $this->mock(QueueServiceInterface::class)
            ->shouldReceive('consume')
            ->with(env('FAILED_SMS_QUEUE'))
            ->andReturn($smsEntity);

        $this->mock(Dispatcher::class)
            ->shouldReceive('dispatchNow')
            ->with(\Mockery::type(SendSmsCommand::class))
            ->andReturnNull();

        $consumeCommand = app(ConsumeFailedSmsCommand::class);
        $consumeCommand->handle();
    }

    public function testDoesntDispatchSendSmsCommandWhenRetryCountIsAbove1(): void
    {
        $smsEntity = SmsEntityFactory::fromArray([
            'uuid' => $this->faker->uuid,
            'type' => 'promotional',
            'phoneNumber' => $this->faker->phoneNumber,
            'messageBody' => $this->faker->realText,
            'failureCount' => 2,
        ]);

        $this->mock(QueueServiceInterface::class)
            ->shouldReceive('consume')
            ->with(env('FAILED_SMS_QUEUE'))
            ->andReturn($smsEntity);

        $this->mock(LoggerInterface::class)
            ->shouldReceive('critical')
            ->with("SMS sending failed, sms with uuid: {$smsEntity->getUuid()} message has been dropped")
            ->andReturnNull();

        $this->mock(Dispatcher::class)
            ->shouldNotReceive('dispatchNow')
            ->with(\Mockery::type(SendSmsCommand::class))
            ->andReturnNull();

        $consumeCommand = app(ConsumeFailedSmsCommand::class);
        $consumeCommand->handle();
    }
}
