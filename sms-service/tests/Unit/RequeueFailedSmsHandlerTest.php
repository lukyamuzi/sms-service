<?php declare(strict_types=1);

namespace Tests\Unit;

use SmsNotifier\Application\Event\SmsSendingFailed;
use SmsNotifier\Application\Handler\RequeueFailedSmsHandler;
use SmsNotifier\UI\CLI\QueueServiceInterface;
use Tests\TestCase;

class RequeueFailedSmsHandlerTest extends TestCase
{
    public function testCanRequeueFailedSmsMessageAfterFailedSmsEvent(): void
    {
        $failedSmsEvent = new SmsSendingFailed(
            'some-uuid',
            'registration confirmation',
            '092878782982',
            'welcome to m-kopa',
            1,
            503,
            'something went wrong',
        );

        $this->mock(QueueServiceInterface::class)
            ->shouldReceive('publish')
            ->with(env('FAILED_SMS_QUEUE'), $failedSmsEvent->toArray())
            ->andReturnNull();

        $handler = app(RequeueFailedSmsHandler::class);

        $handler->handle($failedSmsEvent);
    }
}
